package com.example.datosrouter;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener{

    private Spinner spinner;

    private ArrayList<String> routers;

    private JSONArray result;

    private RequestQueue mQueue;
    TextView nombre1, nombre2;
    TextView brx;
    TextView btx;
    TextView prx;
    TextView ptx;
    TextView fecha1;

    String nombre;
    int RX;
    int TX;
    int PRX;
    int PTX;
    String fecha;

    private String url = "https://telem1.cure.edu.uy/interface/router.php?android=sacar";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        routers = new ArrayList<String>();

        mQueue = Volley.newRequestQueue(this);

        spinner = (Spinner) findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(this);

        getMacs();
    }

    private void getMacs(){
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject j = null;
                        try {
                            j = new JSONObject(response);

                            result = j.getJSONArray("routers");

                            getData(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    private void getData(JSONArray j){
        for(int i=0;i<j.length();i++){
            try {

                routers.add(j.getString(i));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        spinner.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, routers));
    }

    private void ObtenerDatos(int position, String url2){

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url2, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                nombre1 = (TextView) findViewById(R.id.nombre);
                                nombre2 = (TextView) findViewById(R.id.nombre2);
                                brx = (TextView) findViewById(R.id.brx);
                                btx = (TextView) findViewById(R.id.btx);
                                prx = (TextView) findViewById(R.id.prx);
                                ptx = (TextView) findViewById(R.id.ptx);
                                fecha1 = (TextView) findViewById(R.id.fecha);

                                nombre1.setText("");
                                nombre2.setText("");
                                brx.setText("");
                                btx.setText("");
                                prx.setText("");
                                ptx.setText("");
                                fecha1.setText("");

                                for (int i = 0; i < response.length(); i++){
                                    JSONObject interfaz = response.getJSONObject(i);

                                    nombre = interfaz.getString("nombre");
                                    RX = interfaz.getInt("RX");
                                    TX = interfaz.getInt("TX");
                                    PRX = interfaz.getInt("PRX");
                                    PTX = interfaz.getInt("PTX");
                                    fecha = interfaz.getString("fecha");

                                    nombre1.append(nombre + "\n");
                                    brx.append(RX + "\n");
                                    btx.append(TX + "\n");
                                    nombre2.append(nombre + "\n");
                                    prx.append(PRX + "\n");
                                    ptx.append(PTX + "\n");
                                }
                                fecha1.append("Fecha de Actualización: " + fecha + "\n");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            mQueue.add(request);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String mac = "";

        try {

            mac = result.getString(position);

            String url2 = ("https://telem1.cure.edu.uy/interface/router.php?android=" + mac);

            ObtenerDatos(position, url2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}