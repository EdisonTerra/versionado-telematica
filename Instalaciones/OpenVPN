Instalación y Configuración

Proceso de instalación y configuración de certificados:
Lo primero que se debe hacer es verificar que TUN/TAP este instalado y habilitado, se hace con el siguiente comando y se espera el siguiente mensaje OpenVPN utiliza un conjunto de protocolos SSL/TLS que trabajan en la capa de transporte, y tenemos dos tipos de funcionamiento que son los recien nombrados, el TUN que emula un dispositivo punto a punto, es utilizado para crear túneles virtuales operando con el protocolo IP. Y el TAP simula una interfaz de red Ethernet, más comúnmente conocido como modo puente o bridge.
Luego de verificado lo anterior, se procede a instalar openvpn y easy-rsa en el equipo destinado a funcionar como servidor VPN.
      grupo1:~# zypper in openvpn easy-rsa
Una vez hecho esto hay que situarse en el directorio “/etc/openvpn/” y se verifica su contenido, el directorio se encontrará vacío, por lo que se debe copiar el contenido del directorio “/etc/easy-rsa/” al directorio de openvpn.
grupo1:~/etc/openvpn # ls
grupo1:~/etc/opnevpn # cp -r ../easy-rsa/* .
grupo1:~/etc/opnevpn # ls
openssl-easyrsa.cnf safessl-easyrsa.cnf vars.example x509-types
También se debe crear el archivo “vars” usando como base si se desea, el archivo “vars.example”.
grupo1:~/etc/opnevpn # cp vars.example vars
Dentro de este archivo se modifican los siguientes parámetros:
      export KEY_CONFIG=$EASY_RSA/wichopensslcnf/$EASY_RSA
Se debe sustituir por la siguiente línea:
      export KEY_CONFIG=$EASY_RSA/openssl-easyrsa.cnf
Y se des comentan las siguientes líneas:
      export KEY_SIZE=2048
      export KEY_EXPIRE=3650
      export KEY_COUNTRY
      export KEY_PROVINCE
      export KEY_CITY
      export KEY_ORG
      export KEY_EMAIL
      export KEY_OU
La primer linea indica el tamaño de las claves, la segunda el tiempo antes de que expire el certificado, y las demás líneas ofrecen la información visible con la que serás visible desde la red pública.
Lo siguiente es lanzar el comando “easyrsa clean-all”, esto genera un directorio llamado “pki” en el cuál se almacenarán todos los certificados tanto del servidor como los certificados de los clientes.
Una vez hecho esto se debe crear el certificado del servidor el cual puede llevar contraseña o no, se
hace de la siguiente forma:
Para generar certificado y clave para el servidor se debe tirar el siguiente comando “easyrsa build- server-full server”, donde se pedirá una contraseña.
Ahora se debe llevar a cabo el mismo proceso para cada uno de los clientes que se quieran conectar al servidor, es recomendado crear un cliente por cada dispositivo cada uno con sus respectivas llaves, “easyrsa build-client-full ‘nombre del cliente’”
Ejemplo:
Después de tener todo creado se genera el fichero maestro con el comando “easyrsa gen-dh”
Configuración del Servidor:
Ahora se debe configurar el servidor de manera que use los certificados para esto se debe crear el archivo “vim /etc/openvpn/server.conf”, y se toma de Internet cualquier plantilla como por ejemplo:
    • port 1194
    • proto udp
    • management localhost 7505
    • dev tun
    • topology subnet
    • (Aqui se debe especificar la dirección exacta de cada uno de los archivos)
    • ca /etc/easy-rsa/pki/ca.crt
    • cert /etc/easy-rsa/pki/issued/server.crt
    • crl-verify /etc/easy-rsa/pki/crl.pem
    • askpass /etc/easy-rsa/pki/private/server.pass
    • key /etc/easy-rsa/pki/private/server.key
    • dh /etc/easy-rsa/pki/dh.pem
    • #VPN internal IP (Aqui se debe especificar el rango que usara el servidor debiendo ser esta
    • preteneciente a una red privada ejemplo 10.0.0.0/28 o 192.168.0.0/24 entre otra)
    • server 192.168.240.0 255.255.255.0
    • network traffic.
    • push "redirect-private"
    • cipher AES-256-CBC
    • keepalive 20 60
    • comp-lzo
    • persist-key
    • persist-tun
    • daemon
    • status /var/log/openvpn/openvpn-status.log
    • log-append /var/log/openvpn/openvpn.log
    • verb 3
Los archivos importantes que se deben indicar en el archivo anterior son el ca.crt, dh2018.pem, server.key y server.crt.
Por ultimo se inicia el servicio y se configura para que inicie automáticamente:
    • grupo1:~# systemctl start openvpn@server
    • grupo1:~# systemctl enable openvpn@server

Configuración del Firewall

En el archivo que esta en “/etc/sysctl.conf” se debe añadir la siguiente linea:
    • net.ipv4.ip_forward = 1
Luego en la terminal deben llevar a cabo los siguientes comando para especificar que protocolo, red e interfaz se utilizará:
    • firewall-cmd --zone=public –add-port 1194/udp
    • firewall-cmd --zone=public --add-service openvpn
    • firewall-cmd --zone=trusted --add-interface tun0
    • firewall-cmd --zone=trusted --add-masquerade
    • firewall-cmd --direct --passthrough ipv4 -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE
    • firewall-cmd –runtime-to-permanent 
Por ultimo se reinicia el servicio: ‘systemctl restart openvpn@server’

Configuración en el Cliente

Como primer paso se debe tener OpenVpn en los dispositivos del Cliente, este usuario necesita los archivos que contienen las claves para conectarse con el servidor:
    • “ca.crt, usuario.crt, usuario.key”
Los archivos anteriormente listados son enviados mediante SCP y se procede con la creación de un archivo llamado “usuario.ovpn”, y se utilizó la plantilla base con el siguiente contenido, ademas se muestra un ejemplo de plantilla el cual sirve para utilizar el servicio VPN desde el móvil por medio de la aplicación OpenVPN Connect:
    • client
    • remote “IP Servidor” “Puerto”
    • ca "/”Dirección Destino”/ca.crt"
    • cert "/”Dirección Destino”/”Usuario”.cert"
    • key "/”Dirección Destino”/”Usuario”.key"
    • comp-lzo yes
    • cipher AES-256-CBC
    • dev tun
    • proto udp
    • nobind
    • auth-nocache
    • script-security 2
    • persist-key
    • persist-tun
    • remote-cert-tls server
    • 
Móvil
    • 
    • client
    • dev tun
    • proto udp
    • remote “IP Servidor” “Puerto”
    • resolv-retry infinite
    • nobind
    • persist-key
    • persist-tun
    • verb 1
    • keepalive 10 900
    • inactive 3600
    • comp-lzo
    • <ca>
    • Contenido de “ca.crt”
    • </ca>
    • <cert>
    • Contenido de “Usuario.crt”
    • </cert>
    • <key>
    • Contenido de “Usuario.key”
    • </key>
Iniciamos la conexión con el servidor, “sudo openvpn –config usuario.ovpn”.
En la interfaz de red de la maquina debe aparecer:
    • tun0: inet 10.0.0.2 netmask 255.255.255.0 destination 10.0.0.2
