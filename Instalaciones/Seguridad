Implementación HTTPS

En principio para poder realizar la certificación del servicio HTTPs, se tuvo que agregar el nombre de dominio que se otorgo a nuestra dirección IP mediante el siguiente comando:
    • grupo1:/etc # hostnamectl set-hostname telem1.cure.edu.uy
    • grupo1:/etc # hostname telem1.cure.edu.uy
También se debe agregar ‘telem1.cure.edu.uy’ al final del archivo ‘/etc/hosts’ de la siguiente manera: 164.73.226.11 telem1.cure.edu.uy grupo1
Luego se procedió instalando certbot el cual es un cliente de Let's Encrypt (autoridad de certificación gratuita) que para pedir o renovar certificados, y se hizo de la siguiente manera:
    • telem1:/etc # zypper install certbot python-certbot-apache
Una vez instalados todos los paquetes se debe proceder ejecutando el comando que realizará la configuración del certificado para el servicio HTTPs:
    • telem1:/etc # certbot --apache
Esto comenzá a realizar una serie de consultas y preguntas que se deben responder para obtener dicho certificado, hay que tener en cuenta que como requisito previo certbot solicita tener un virtual host configurado en el puerto 80 para que este pueda asegurar que tiene acceso al dominio de con el que se esta trabajando.
Otro requisito para poder utilizar el servicio por el puerto 443 (https), se debe verificar que en el archivo ‘/etc/sysconfig’ se encuentren los siguientes campos: APACHE_MODULES="……...ssl"
En el apartado APACHE_MODULES este incluido el campo ‘ssl’ y también verificar que en el apartado APACHE_SERVER_FLAGS se encuentre con la siguiente información APACHE_SERVER_FLAGS="-D SSL"
Por último se procede con la configuración de los virtual hosts, primero se debe realizar una copia del archivo vhost-ssl.template que se encuentra en el directorio ‘/etc/apache2/vhosts.d/’
    • telem1:/etc/apache2/vhosts.d # cp vhost-ssl.template vhost-ssl.conf
Dentro del archivo vhost-ssl.conf se debe configurar los siguientes parámetros:
DocumentRoot "/srv/www/" → Dirección donde se encuentra la configuración del servicio web
ServerName telem1.cure.edu.uy → nombre de domino del servidor
SSLCertificateFile /etc/letsencrypt/live/telem1.cure.edu.uy/fullchain.p$ → claves de certbot
SSLCertificateKeyFile /etc/letsencrypt/live/telem1.cure.edu.uy/privkey.$

LDAP sorbe SSL

Para habilitar LDAPs lo que se hizó fue copiar los certificados creados por certboot que se encuentran en el directorio ‘/etc/letsencrypt/archive/’ hacia el directorio de openldap para poder cambiar los permisos de usuario sin que hubiera conflictos entre los mismos. Ya ubicados los archivos en ‘/etc/openldap/ssl/’se procedió cambiando los permisos con el comando:
    • telem1:/etc/openldap/ssl # chown ldap:ldap telem1.cure.edu.uy/
Una vez hecho esto se paso a indicar en el archivo slapd.conf ubicado en /etc/openldap donde se encuentran estos certificados:
    • TLSCACertificateFile /etc/openldap/ssl/telem1.cure.edu.uy/fullchain1.pem
    • TLSCertificateFile /etc/openldap/ssl/telem1.cure.edu.uy/cert1.pem
    • TLSCertificateKeyFile /etc/openldap/ssl/telem1.cure.edu.uy/privkey1.pem
Para comprobar el funcionamiento de LDAP sobre SSL/TLS, se recurrió a la herramienta ldapsearch y realizar una búsqueda sobre el puerto 636:
    • ldapsearch -x -H ldaps://telem1.cure.edu.uy:636 -b "dc=grupo1,dc=com" "cn=*castillos*"
Luego dentro de NextCloud accedemos a la configuración de LDAP y en la misma se debe cambiar el puerto por el 636 y se debe indicar que trabaje con SSL mediante “ldaps://telem1.cure.edu.uy”.


Implementación de Firmas Digitales

Para esto es necesario instalar dicho paquete en nuestro equipo utilizando el siguiente comando:
    • apt install openssl
Una vez instalado el paquete, se debe crear ciertos archivos y directorios dentro de “/etc/ssl” que serán utilizados por el archivo de configuración “openssl.cnf”, se crea el siguiente directorio los siguientes archivos::
    • mkdir newcerts && echo ‘01’ > serial && touch index.txt
En el archivo de configuraciones “openssl.cnf” debemos indicarle el directorio sobre el que se está trabajando modificando el valor de la variable “dir” en la siguiente línea:
    • dir = ./DemoCA/
Introduciendo el directorio de trabajo:
    • dir = /etc/ssl/
Una vez realizados estos pasos previos, se procede con la creación de los certificados de la autoridad certificadora, ejecutando el siguiente comando:
root@equipo:/etc/ssl# openssl req -new -x509 -extensions v3_ca -keyout private/cakey.pem -out cacert.pem -days 365 -config ./openssl.cnf
Ingresando una contraseña solicitada y llenando los campos que se requieren en dicho proceso. En base a estos certificados, se crea el certificado digital utilizando los siguientes comandos:
    • openssl req -new -nodes -out “Nombre certificado de clave privada”.pem -config ./openssl.cnf
    • openssl ca -out “Nombre certificado de clave pública”.pem -config ./openssl.cnf -days 365 -infiles “Nombre certificado de clave privada”.pem
En cada comando se solicitará la contraseña de la autoridad certificadora y luego una para los certificados que se están creando, y se solicitará información adicional como se muestra a continuación:
      Country Name (2 letter code) [AU]: UY
      State or Province Name (full name) [Some-State]: Rocha
      Locality Name (eg, city) []: Castillos
      Organization Name (eg, company) [Internet Widgits Pty Ltd]: Telem1
      Organizational Unit Name (eg, section) []: grupo1
      Common Name (e.g. server FQDN or YOUR name) []: Usuario
      Email Address []: usuario@mail.com
Para usar el certificados de clave pública como firma digital, es necesario pasarlo de formato “.pem” a un formato soportado como por ejemplo “.p12” o “.pfx”. Esto se puede realizar con el siguiente comando:
root@equipo:/etc/ssl# openssl pkcs12 -inkey privkey.pem -in “Nombre certificado de clave pública”.pem -export -out “Nombre certificado para firma”.p1
Para firmar un documento PDF, se puede hacer uso de la herramienta de software libre “AutoFirma”, para obtenerla, dentro de un directorio vacío ejecutamos la siguiente línea:
    • wget http://estaticos.redsara.es/comunes/autofirma/currentversion/AutoFirma_Linux.zip
Una vez descargado el paquete se debe descomprimir e instalar de la siguiente forma:
    • unzip AutoFirma_Linux.zip && dpkg -i *.deb
