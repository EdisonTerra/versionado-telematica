<?php include("seguridad.php"); ?>

<?php
$servidor= "localhost";
$usuario= "user";
$password = "passuser";
$nombreBD= "nombredb";

$conexion = new mysqli($servidor, $usuario, $password, $nombreBD);
if ($conexion->connect_error) {
    die("No se pudo conectar con la base: " . $conexion->connect_error);
}

$forbidden = array("¿","?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "´", "\"", "&",
"$", "#", "*", "(", ")" , "|", "~", "`","¡", "!", "{", "}", "%", "+" , chr(0));

$consulta = $conexion->prepare("INSERT INTO routers VALUES (?,?,?,?)");
$consulta->bind_param("ssss", $router, $modelo, $fecha, $hora);

$router = str_replace($forbidden,"", $_POST['router']);
$modelo = str_replace($forbidden,"", $_POST['modelo']);
$fecha = $_POST['fecha'];
$hora = $_POST['hora'];
$consulta->execute();
$consulta->close();
?>

<html>
    <head>
        <title>Mostrar datos</title>
    </head>
    <body>
        <link rel="stylesheet" href="estilotabla.css">
        <section class="form-autenticar">

        <h2>Tabla de Routers</h2>
    <table border="1px">
  <thead>
        <tr>
            <th><h4>Router</h4></th>
            <th><h4>Modelo</h4></th>
            <th><h4>Fecha</h4></th>
            <th><h4>Hora</h4></th>

        </tr>
         </thead>
        <?php
            $sql= "SELECT router,modelo,fecha,hora from routers";
            $result= mysqli_query($conexion,$sql);

        while($mostrar=mysqli_fetch_array($result)){
        ?>

        <tr>
            <td><h5><?php echo $mostrar['router'] ?></h5></td>
            <td><h5><?php echo $mostrar['modelo'] ?></h5></td>
            <td><h5><?php echo $mostrar['fecha'] ?></h5></td>
            <td><h5><?php echo $mostrar['hora'] ?></h5></td>
        </tr>
        <?php
        }
	$conexion->close();
        ?>
    </table>
        <br>
        <form action="register.php">
        <input class="boton2" type="submit" value="Registrar">
        </form>
        <form action="elegir.php">
        <input class="boton2" type="submit" value="Volver">
        </form>


        </section>

    </body>
</html>

	
